La variable d'environnement `$NEXUS` est nécessaire pour se connecter au registre de La Forge.

La variable d'environnement `$TAG` est nécessaire pour télécharger la bonne version de l'image.

Le fichier `docker/pythonpath_dev/superset_config_docker.py` contient la configuration de Superset.

Pour lancer Superset 3.0 (sur le port 8088):
```bash
export NEXUS=nexus.forge-dc.cloudmi.minint.fr
export TAG=3.0.0
docker-compose -f docker-compose-non-dev.yml up -d
```

Config nginx dans `/etc/nginx/sites-enabled/superset` pour rediriger sur le port 80:
```
#/etc/nginx/sites-enabled/superset
server {
  listen 80;
  client_max_body_size 4G;
  access_log /var/log/nginx/superset_access.log;
  error_log /var/log/nginx/superset_error.log;

  keepalive_timeout 5;

  location / {
    include proxy_params;
    proxy_pass http://localhost:8088;
  }
}
```

Puis recharger la config:
```bash
sudo nginx -s reload
```

Se connecter avec le compte admin/admin.
